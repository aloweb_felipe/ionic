<?php

namespace App\Http\Controllers\Api\Deliveryman;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Repositories\OrderRepository;
use App\Repositories\UserRepository;
use App\Services\OrderService;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;

class DeliverymanCheckoutController extends Controller
{
    /**
     * @var OrderRepository
     */
    private $repository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var OrderService
     */
    private $service;

    private $with = ['client','cupom','items'];

    public function __construct(OrderRepository $repository,
                                UserRepository $userRepository,
                                OrderService $service)
    {
        $this->repository = $repository;
        $this->userRepository = $userRepository;
        $this->service = $service;
    }

    public function index()
    {
        $id = Authorizer::getResourceOwnerId();
        $orders = $this->repository->skipPresenter(false)->with($this->with)->scopeQuery(function($query) use($id){
            return $query->where('user_deliveryman_id','=',$id);
        })->paginate();

        return $orders;
    }

    public function show($id){

        $idDeliveryman = Authorizer::gerResourceOwnerId();
        return $this->repository->skipPresenter(false)->getByIdAndDeliveryMan($id,$idDeliveryman);

    }

    public function updateStatus(Requests $requests, $id){
        $idDeliveryman = Authorizer::gerResourceOwnerId();
        $order = $this->service->updateStatus($id, $idDeliveryman, $requests->get('status'));
        if($order){
            return $this->repository->find($order->id);
        }
        abort(400, 'Order não encontrado');
    }


}
